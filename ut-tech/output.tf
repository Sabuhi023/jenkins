output "registry_id" {
    description = "it is a repository id"
    value = aws_ecr_repository.f-repository.registry_id
  
}
output "repository_url" {
    description = "url for repository"
    value = aws_ecr_repository.f-repository.repository_url
  
}
output "rds_hostname" {
    description = "rds instance histname"
    value = aws_db_instance.rds_instance.address
  
}
output "rds_port" {
    description = "rds instance port"
    value = aws_db_instance.rds_instance.port
  
}
output "rds_username" {
    description = "rds instance root username"
    value = aws_db_instance.rds_instance.username
      
    
  
}