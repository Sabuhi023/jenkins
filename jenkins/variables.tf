variable "instance_name" {
    default = "ec2 instance for test"
}
variable "instance_type" {
    default = "t2.micro"
  
}

variable "vpc_cidr_block" {
    default = "10.0.0.0/16"
  
}

variable "prefix" {
  
  default = "test"
}

variable "subnet_cidr_block" {
  default = "10.0.10.0/24"
}

variable "private_subnets"  {
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  }

variable "public_subnets"  {
  default = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  }

variable "azs" {
  default = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
}

variable "my_ip" {
   default =  "5.197.250.9/32"

}